# Pigeon

A (soon to be) on-the-go remote file sharing program.

## Quick Start
```sh
$ git clone https://gitlab.com/daisyflare/pigeon
$ cargo build --release && cp target/release/pigeon .
$ ./pigeon
```

Requires [Rust](https://rustup.rs) installed.
