use slice_diff_patch as sdp;
use std::sync::mpsc;

pub struct Diff {
    pub changes: Vec<sdp::Change<String>>,
}

impl From<Diff> for Vec<u8> {
    fn from(d: Diff) -> Self {
        let mut v = Vec::new();
        let mut char_buf = [0u8; 4];
        for line in d.changes {
            match line {
                sdp::Change::Remove(r) => {
                    let s = '-'.encode_utf8(&mut char_buf);
                    v.extend(s.as_bytes());
                    v.extend(r.to_string().as_bytes());
                    let s = '\n'.encode_utf8(&mut char_buf);
                    v.extend(s.as_bytes());
                }
                sdp::Change::Insert((idx, item)) => {
                    let s = 'i'.encode_utf8(&mut char_buf);
                    v.extend(s.as_bytes());
                    v.extend(idx.to_string().as_bytes());
                    let s = 'i'.encode_utf8(&mut char_buf);
                    v.extend(s.as_bytes());
                    v.extend(item.as_bytes());
                    let s = '\n'.encode_utf8(&mut char_buf);
                    v.extend(s.as_bytes());
                }
                sdp::Change::Update((idx, item)) => {
                    let s = 'u'.encode_utf8(&mut char_buf);
                    v.extend(s.as_bytes());
                    v.extend(idx.to_string().as_bytes());
                    let s = 'u'.encode_utf8(&mut char_buf);
                    v.extend(s.as_bytes());
                    v.extend(item.as_bytes());
                    let s = '\n'.encode_utf8(&mut char_buf);
                    v.extend(s.as_bytes());
                }
            }
        }
        v
    }
}

impl From<Vec<u8>> for Diff {
    fn from(other: Vec<u8>) -> Self {
        let s = String::from_utf8(other).unwrap();
        let mut changes = Vec::new();
        for line in s.lines() {
            let change = match line.chars().next() {
                Some('-') => {
                    let num = line[1..].parse().unwrap();
                    sdp::Change::Remove(num)
                }
                Some('i') => {
                    let num_buf = line[1..]
                        .chars()
                        .take_while(|c| c.is_numeric())
                        .collect::<String>();
                    let num = num_buf.parse::<usize>().unwrap();
                    sdp::Change::Insert((num, line[(num_buf.len() + 2)..].to_string()))
                }
                Some('u') => {
                    let num_buf = line[1..]
                        .chars()
                        .take_while(|c| c.is_numeric())
                        .collect::<String>();
                    let num = num_buf.parse::<usize>().unwrap();
                    sdp::Change::Update((num, line[(num_buf.len() + 2)..].to_string()))
                }
                _ => panic!(),
            };
            changes.push(change);
        }

        Self { changes }
    }
}

impl Diff {
    pub fn new(start: &str, end: &str) -> Option<Self> {
        if start == end {
            return None;
        }

        let v1: Vec<String> = start.lines().map(str::to_string).collect();
        let v2: Vec<String> = end.lines().map(str::to_string).collect();

        let diff = sdp::diff_diff(&v1, &v2);

        Some(Diff { changes: diff })
    }
}

pub trait SendDiff {
    type Error;
    fn send_diff(&mut self, diff: Vec<u8>) -> Result<(), Self::Error>;
}

impl SendDiff for mpsc::Sender<Vec<u8>> {
    type Error = mpsc::SendError<Vec<u8>>;
    fn send_diff(&mut self, diff: Vec<u8>) -> Result<(), mpsc::SendError<Vec<u8>>> {
        self.send(diff)
    }
}

pub trait RecvDiff {
    type Error;

    fn recv_diff(&mut self) -> Result<Vec<u8>, Self::Error>;

    fn try_recv_diff(&mut self) -> Result<Vec<u8>, Option<Self::Error>>;
}

impl RecvDiff for mpsc::Receiver<Vec<u8>> {
    type Error = mpsc::RecvError;

    fn recv_diff(&mut self) -> Result<Vec<u8>, Self::Error> {
        self.recv()
    }

    fn try_recv_diff(&mut self) -> Result<Vec<u8>, Option<Self::Error>> {
        match self.try_recv() {
            Ok(o) => Ok(o),
            Err(e) => {
                if let mpsc::TryRecvError::Disconnected = e {
                    Err(Some(self.recv().unwrap_err()))
                } else {
                    Err(None)
                }
            }
        }
    }
}
