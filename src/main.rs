use std::{env, fs, io::Write, sync::mpsc, thread, time};

mod diff;

use diff::SendDiff;
use notify::{self, Watcher};
use slice_diff_patch as sdp;

use crate::diff::{Diff, RecvDiff};

fn main() {
    let (mut diff_tx, rx) = mpsc::channel();

    let path = "reader.txt";

    let fs_rx = watch_relative_file(path);

    let mut buf = fs::read_to_string(&path).unwrap();

    let handle = spawn_writer_thread(&buf, rx);

    while let Ok(fevent) = fs_rx.recv() {
        if let notify::DebouncedEvent::Write(_) = fevent {
            let buf2 = fs::read_to_string(&path).unwrap();

            if let Some(diff) = Diff::new(&buf, &buf2) {
                diff_tx.send_diff(diff.into()).unwrap();
                buf = buf2;
            }
        }
    }

    handle.join().unwrap();
}

fn watch_relative_file(p: &str) -> mpsc::Receiver<notify::DebouncedEvent> {
    let (tx, fs_rx) = mpsc::channel();

    let mut watcher = notify::watcher(tx, time::Duration::from_millis(500)).unwrap();

    let mut path = env::current_dir().unwrap();
    path.push(p);

    watcher
        .watch(&path, notify::RecursiveMode::NonRecursive)
        .unwrap();
    fs_rx
}

fn spawn_writer_thread<T: RecvDiff + Send + 'static>(
    buf: &str,
    mut rx: T,
) -> thread::JoinHandle<()> {
    let b_end_rn = buf.ends_with("\r\n");
    let b_end_n = buf.ends_with('\n');

    thread::spawn(move || {
        let mut buf = Vec::new();

        while let Ok(data) = rx.recv_diff() {
            let diff = Diff::from(data);
            buf = sdp::patch(&buf, &diff.changes);

            let mut path = env::current_dir().unwrap();
            path.push("writer.txt");
            let mut f = fs::File::create(path).unwrap();

            let mut t = buf.join("\n");
            if b_end_rn {
                t += "\r\n";
            } else if b_end_n {
                t += "\n";
            }

            f.write_all(t.as_bytes()).unwrap();
        }
    })
}
